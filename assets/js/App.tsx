import React, { useContext } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import GameListPage from "./pages/GameListPage";
import GameDetailPage from "./pages/GameDetailPage";


const App = () => {

	return (
		<React.Fragment>
			<Routes>
			<Route path="/" element={<GameListPage />} />
			<Route
				path="/:gameId"
				element={<GameDetailPage />}
			/>
			</Routes>
		</React.Fragment>
	);
};

export default App;
