import React, { useContext, useEffect, useState } from "react";
import Layout from "../components/Layout";
import { Link, useParams } from "react-router-dom";
import gamesList from "../games.json";


const ProductDetailPage = () => {
    const params = useParams();
    const [game, setGame] = useState<{}>();

    const { gameId } = params;

    useEffect(() => {
        getGameApi();
    }, []);

    const getGameApi = async () => {
        const gameSingle = gamesList.find(game => game.id === gameId);
        setGame(gameSingle);
    };

    return (
        <Layout>
            <section className="games-single-page">
		<div className="container">
			<div className="game-single-preview">
				<img src={game?.background} alt=""/>
			</div>
			<div className="row">
				<div className="col-xl-9 col-lg-8 col-md-7 game-single-content">
					<div className="gs-meta">11.11.18  /  in <a href="">Games</a></div>
					<h2 className="gs-title">{game?.name}</h2>
					<h4>Gameplay</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquamet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vestibulum posuere porttitor justo id pellentesque. Proin id lacus feugiat, posuere erat sit amet, commodo ipsum. Donec pellentesque vestibulum metus.</p>
					<h4>Conclusion</h4>
					<p>Nulla ut maximus mauris. Sed malesuada at sapien sed euismod. Vestibulum pharetra in sem id laoreet. Cras metus ex, placerat nec justo quis, luctus posuere ex. Vivamus volutpat nibh ac sollicitudin imperdiet. Donec scelerisque lorem sodales odio ultricies, nec rhoncus ex lobortis. Vivamus tincidunt sit amet sem id varius. Donec ele-mentum aliquet tortor. Curabitur justo mi, efficitur sed eros aliquet, dictum molestie eros. Nullam scelerisque convallis gravida. Morbi id lorem accumsan, scelerisque enim laoreet, sollicitudin neque. Vivamus volutpat nibh ac sollicitudin imperdiet. Donec scelerisque lorem sodales odio ultricies, nec rhoncus ex lobortis. Vivamus tincidunt sit amet sem id varius. Donec ele-mentum aliquet tortor. Curabitur justo mi, efficitur sed eros aliqueDonec vitae tellus sodales, congue augue at, biben-dum justo. Pellentesque non dolor et magna volutpat pharetra eget vel ligula. Maecenas facilisis vestibulum mattis. Sed sagittis gravida urna. Cras nec mi risus.
					</p>
                </div>	
			</div>
		</div>
	</section>
          
        </Layout>
    );
};

export default ProductDetailPage;
