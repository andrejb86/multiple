import React, { useContext, useEffect, useState } from "react";
import Layout from "../components/Layout";
import { Link } from "react-router-dom";
import gamesList from "../games.json";

const ProductListPage = () => {
    const [games, setGames] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [newGames, setNewGames] = useState([]);

    useEffect(() => {
        if(searchText === "") 
            getProductsApi();
        else 
            searchGames();
    }, [games]);

    const getProductsApi = async () => {
        
        setGames(gamesList)

    };

    const searchGames = () => {
        const newGames= games.filter(game => { 
            return game.name.toLowerCase().includes(searchText.toLowerCase()) || game.provider.toLowerCase().includes(searchText.toLowerCase())
        })
        setGames(newGames);
        setNewGames(newGames);
    }

    const onSubmit = (e) => {
        e.preventDefault();
        searchGames();
    }


    return (
        <Layout>
          	<section className="games-section h-100" >
		        <div className="container">
                <div className="form-outline mb-4">
                    <form onSubmit={onSubmit}>
                        <div className="d-flex align-items-center mb-3 pb-1">
                            <span className="h1 fw-bold mb-0 text-light">{__APP__.NAME}</span>
                        </div>
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Search games..."
                            onChange={(e) => setSearchText(e.target.value)}
                            
                        />
            
                    </form>
                </div>
               
                <div className="row">
                {games ? games.map((game) => (
						<div className="col-lg-4 col-md-6">
							<div className="game-item">
								<img className="game-image" src={game["icon_2"]} alt={game.name}/>
								<h5>{game.name}</h5>
								<Link
                                    to={`/${game.id}`}
                                    className="btn btn-secondary"
                                >
                                    <i className="bi bi-eye-fill" /> Detail
                                </Link>
							</div>
						</div>
                    )) : (<p>No game found...</p>)}
						 </div>
                   </div>
	         </section>
        </Layout>
    );
};

export default ProductListPage;
